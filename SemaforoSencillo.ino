/*Semaforo de dosp untos, sencillo
Semaforo Punto 1 --> A
Semaforo Punto 2 --> B
*/

#define  AR 2 // Semaforo A, led Rojo
#define  AA 3
#define  AV 4
#define  BR 5 // Semaforo B, led Rojo
#define  BA 6
#define  BV 7

void setup() 
{
  //se definen como de entrada y salida los pines
  pinMode(AR, 1); // 1 --> OUTPUT
  pinMode(AA, 1); 
  pinMode(AV, 1);
  pinMode(BR, 1);
  pinMode(BA, 1);
  pinMode(BV, 1);
}

void loop() 
{
  digitalWrite(AR, HIGH);
  digitalWrite(BR, HIGH);
  delay(1000);

  //Semaforo A inicia
  digitalWrite(AR, LOW);
  digitalWrite(AA, HIGH);
  delay(500);
  digitalWrite(AA, LOW);
  digitalWrite(AV, HIGH);
  delay(5000);
  digitalWrite(AV, LOW);

  //Cambio de Semaforo 1 a 2
  digitalWrite(AA, HIGH);
  delay(500);
  digitalWrite(AA, LOW);
  digitalWrite(AR, HIGH);
  
  
  //Semaforo 2
  digitalWrite(BR, LOW);
  digitalWrite(BA, HIGH);
  delay(500);
  digitalWrite(BA, LOW);
  digitalWrite(BV, HIGH);
  delay(5000);
  digitalWrite(BV, LOW);

  //Cambio de Semaforo 2 a 1
  digitalWrite(BA, HIGH);
  delay(500);
  digitalWrite(BA, LOW);
  
  

}
